INTRODUCTION
------------

Slideshow Block can be used to create a slideshow of images within a block on
any and/or every page of a site. Powered by Views and jQuery, slideshows can be
customised for each individual page to play automatically, display a single
image or be hidden altogether depending on the needs of the page.

 * For a full description of the module, visit the project page:
   http://drupal.org/project/slideshow_block

 * To submit bug reports and feature suggestions, or to track changes:
   http://drupal.org/project/issues/slideshow_block

REQUIREMENTS
------------
This module requires the following modules:
 * Views (https://drupal.org/project/views)
 * Views Slideshow (https://www.drupal.org/project/views_slideshow)

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Configure user permissions in Administration » People » Permissions:

   - Administer Slideshow Block

     Users with this permission will be able to access the module settings page
     and change the way the slideshow behaves throughout the site.

 * Create a taxonomy that will contain the slides in Structure » Taxonomy » Add
   vocabulary. This taxonomy needs to have an image field to hold the slide
   image and a text field for the description.

 * Customise the module settings in Administration » Content authoring »
   Slideshow Block settings:
   
    - Slideshow taxonomy
      
      Select the taxonomy that was created in the previous step.
      
    - Display options
    
      Enter the slide width and height dimensions (all slides will be made to
      fit these dimensions) and choose whether to use the included stylesheet
      and whether to display the slide descriptions when the user moves their
      mouse over the block.
      
    - Default display mode
      
      Choose which mode the slideshow should use by default (i.e. when it is
      not overridden by on-page settings).
      
    - Enable custom slideshow settings
      
      Select which page types are allowed to override the default display mode.
      
    - Node/Path/Taxonomy/Views settings
      
      Choose display modes for each different node type, individual paths,
      taxonomies and views.
      
 * Customise the slideshow settings for nodes and node types.
 
   Slideshow Blocks can be customised for different node types by enabling them
   in the module settings (above) and then navigating to the content type edit
   form. This form also allows individual nodes of this content type to have
   different display options, which are set in the node edit form.
 
 * Enable the Slideshow Block in Structure » Blocks
    
MAINTAINERS
-----------
Current maintainers:
 * David Allen (maestroal) - http://drupal.org/user/2987519