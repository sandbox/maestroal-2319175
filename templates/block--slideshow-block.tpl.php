<?php

/**
 * @file
 * Slideshow Block module implementation to display the slideshow block.
 *
 * @see block.tpl.php
 */

?>
<?php if ($content): ?>
<div<?php print $attributes; ?>>
  <div class="content<?php print ($show_text ? ' slideshow-descriptions' : ''); ?>">
    <?php print $content ?>
    <div style="padding-top:<?php print round(($slide_height / $slide_width) * 100, 2); ?>%;"></div><?php // <== This is the spacer block (do not remove!) ?>
  </div>
</div>
<?php endif; ?>
