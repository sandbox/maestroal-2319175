<?php
/**
 * @file
 * Provides a Slideshow Block display extender plugin for View 3.x.
 */
class slideshow_block_display_extender extends views_plugin_display_extender {

  protected function enabled() {
    return slideshow_block_settings('types/views/status') && $this->display->has_path();
  }

  function options_summary(&$categories, &$options) {
    if ($this->enabled()) {
      $categories['slideshow_block'] = array(
        'title' => t('Slideshow Block'),
        'column' => 'second',
      );
      $slideshowMode = $this->display->get_option('slideshow_block_mode');
          if (empty($slideshowMode)) {
        $slideshowMode = slideshow_block_settings('default/mode');
      }
      $options['slideshow_block'] = array(
        'category' => 'slideshow_block',
        'title' => t('Display mode'),
        'value' => ucfirst($slideshowMode),
      );
    }
  }

  function options_form(&$form, &$form_state) {
    if ($form_state['section'] == 'slideshow_block') {
      $form['#title'] .= t('Slideshow Block');
      $slideshowMode = $this->display->get_option('slideshow_block_mode');
      $slideshowSingle = $this->display->get_option('slideshow_block_single');
      if (empty($slideshowMode)) {
        $slideshowMode = slideshow_block_settings('default/mode');
      }
      if (empty($slideshowSingle)) {
        $slideshowSingle = slideshow_block_settings('default/single');
      }
      $form['slideshow_block'] = slideshow_block_form_builder('views', $slideshowMode, $slideshowSingle);
      $form['slideshow_block']['#type'] = 'container';
    }
  }

  function options_submit(&$form, &$form_state) {
    if ($form_state['section'] == 'slideshow_block') {
      $this->display->set_option('slideshow_block_mode', $form_state['values']['slideshow_block_views_mode']);
      $this->display->set_option('slideshow_block_single', $form_state['values']['slideshow_block_views_single']);
    }
  }
}